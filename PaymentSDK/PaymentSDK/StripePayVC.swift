//
//  StripePayVC.swift
//  PaymentSDK
//
//  Created by Newt on 07/07/21.
//

import UIKit
import Stripe

class StripePayVC: UIViewController {

    @IBOutlet weak var txtAmount: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:-Action when tap pay
    @IBAction func btnPayNowTapped(_ sender: Any) {
        if txtAmount.text?.isEmpty == false {
            //creating controller object of STPAddCardViewController to open card add screen
            let addCardVC = STPAddCardViewController()
            //add delegate to get response after adding card valid details
            addCardVC.delegate = self
            self.navigationController?.pushViewController(addCardVC, animated: true)
        } else {
            displayAlert(title: "", message: "Please enter amount")
        }
    }
    
    //MARK:- Payment Api
    func completePaymentProcessApi(_ stripeId: String, _ error: STPErrorBlock?) {
       //send stripe id with require other details to backend to complete the payment process
        if error != nil {
            print(stripeId)
        } else {
            displayAlert(title: "", message: "error")
        }
    }
    
    func displayAlert(title: String, message: String, restartDemo: Bool = false) {
      DispatchQueue.main.async {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        self.present(alert, animated: true, completion: nil)
      }
    }
}

extension StripePayVC: STPAddCardViewControllerDelegate {
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreatePaymentMethod paymentMethod: STPPaymentMethod, completion: @escaping STPErrorBlock) {
        //getting stripe id
        completePaymentProcessApi(paymentMethod.stripeId, completion)
        self.navigationController?.popViewController(animated: true)
    }
}
