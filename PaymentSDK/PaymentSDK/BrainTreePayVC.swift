//
//  BrainTreePayVC.swift
//  PaymentSDK
//
//  Created by Newt on 07/07/21.
//

import UIKit
import Braintree
import BraintreeDropIn

class BrainTreePayVC: UIViewController {
    
    @IBOutlet weak var txtAmount: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func paymentCompleteApi(_ nonce: String, _ amount: String) {
        //you have to send this to backend to complete the payment
        print(nonce,amount)
    }
    
    //Action when you start payment process
    @IBAction func btnPayNowTapped(_ sender: Any) {
        if txtAmount.text?.isEmpty == false {
            //create request for getting nonce
            let request =  BTDropInRequest()
            let dropIn = BTDropInController(authorization: Constants.toKinizationKey, request: request)
            { (controller, result, error) in
                if (error != nil) {
                    print("ERROR")
                } else if (result?.isCanceled == true) {
                    print("CANCELED")
                } else if let result = result {
                    if let nonce = result.paymentMethod?.nonce {
                        //Method for sending nonce to backend to process payment
                        self.paymentCompleteApi(nonce, self.txtAmount.text ?? "")
                    }
                }
                controller.dismiss(animated: true, completion: nil)
            }
            self.present(dropIn!, animated: true, completion: nil)
        } else {
            displayAlert(title: "", message: "Please enter amount")
        }
    }
    
    func displayAlert(title: String, message: String, restartDemo: Bool = false) {
      DispatchQueue.main.async {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel))
        self.present(alert, animated: true, completion: nil)
      }
    }
}
